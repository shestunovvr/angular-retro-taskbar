#Link to deployed project
https://angular-retro-b7866.web.app/

# Some features
- user can register/login/logout 
- user can add new columns (types of tasks) 
- user can add tasks to any column 
- user can drag-and-drop tasks between columns 
- user can like/unlike tasks 
- user can see and leave comments to tasks 
- every action saves to firestore and app performs based on firestore DB 
- user can export taskbar to external excel file.
