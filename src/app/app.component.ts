import {Component, DoCheck, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from "./services/auth.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'angular-retro'
  currentRoute: any
  currentUser: string | null
  currentUserSubscription: Subscription

  constructor(
    public auth: AuthService
  ) {
  }

  ngOnInit(): void {
    this.currentUserSubscription = this.auth.currentUser.subscribe(user => {
      this.currentUser = user;
    })
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }
}
