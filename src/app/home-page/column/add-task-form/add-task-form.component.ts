import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-task-form',
  templateUrl: './add-task-form.component.html',
  styleUrls: ['./add-task-form.component.scss']
})
export class AddTaskFormComponent implements OnInit {
  @Output() onSubmit: EventEmitter<string> = new EventEmitter<string>()
  @Output() onClose: EventEmitter<void> = new EventEmitter<void>()
  form: FormGroup
  constructor() { }

  ngOnInit(): void {
    this.setForm();
  }

  submit(): void {
    if(this.form.valid) {
      this.onSubmit.emit(this.form.value.text);
      this.form.reset();
    }
  }

  closeForm(): void {
    this.onClose.emit();
  }

  setForm(): void {
    this.form = new FormGroup({
      text: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)
      ])
    })
  }

}
