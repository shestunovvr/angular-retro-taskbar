import {Component, DoCheck, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Column, Item} from "../../shared/interfaces";

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss']
})
export class ColumnComponent implements OnInit {
  @Input() column: Column
  @Input() tasks: Item[]
  @Input() currentUser: string | null
  @Output() toggleItemComments: EventEmitter<{item: Item, column: Column}> = new EventEmitter<{item: Item, column: Column}>()
  @Output() onNewTaskSubmit: EventEmitter<{text: string, column: Column}> = new EventEmitter<{text: string, column: Column}>()
  @Output() onLikesClick: EventEmitter<{item: Item, column: Column}> = new EventEmitter<{item: Item, column: Column}>()
  @Output() onCommentFormSubmit: EventEmitter<{id: string, commentText: string, columnTitle: string}> = new EventEmitter<{id: string, commentText: string, columnTitle: string}>()
  showForm = false;
  color: string
  bgColor: string

  constructor() { }

  ngOnInit(): void {
    this.setColors();
  }

  onCommentsClick(item: Item): void {
    this.toggleItemComments.emit({item: item, column: this.column});
  }

  showAddTaskForm(): void {
    this.showForm = true;
  }

  newTaskSubmit(text: string): void {
    this.onNewTaskSubmit.emit({text: text, column: this.column});
    this.showForm = false;
  }

  likesClick(item: Item): void {
    this.onLikesClick.emit({item: item, column: this.column});
  }

  commentSubmit(event: { id: string, commentText: string }): void {
    this.onCommentFormSubmit.emit(
      {
        id: event.id,
        commentText: event.commentText,
        columnTitle: this.column.title
      });
  }

  closeForm(): void {
    this.showForm = false;
  }

  setColors(): void {
    if(this.column.color) {
      this.color = this.column.color;
    } else {
      this.color = 'black';
    }
    if(this.column.bgColor) {
      this.bgColor = this.column.bgColor;
    } else {
      this.bgColor = 'teal';
    }
  }
}
