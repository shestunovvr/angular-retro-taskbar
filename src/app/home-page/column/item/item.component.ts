import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Item} from "../../../shared/interfaces";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() item: Item
  @Input() currentUser: string | null
  @Output() toggleItemComments: EventEmitter<Item> = new EventEmitter<Item>()
  @Output() onLikesClick: EventEmitter<Item> = new EventEmitter<Item>()
  @Output() onCommentFormSubmit: EventEmitter<{ id: string, commentText: string }> = new EventEmitter<{ id: string, commentText: string }>()

  commentForm: FormGroup

  constructor() {
  }

  ngOnInit(): void {
    this.setForm();
  }

  onCommentsClick(item: Item): void {
    this.toggleItemComments.emit(item);
  }

  likesClick(item: Item): void {
    this.onLikesClick.emit(item);
  }

  submit(id: string): void {
    if (this.commentForm.valid) {
      this.onCommentFormSubmit.emit(
        {
          id: id,
          commentText: this.commentForm.value.text
        }
      );
      this.commentForm.reset();
    }
  }

  setForm(): void {
    this.commentForm = new FormGroup({
      text: new FormControl('', [
        Validators.required
      ])
    })
  }
}
