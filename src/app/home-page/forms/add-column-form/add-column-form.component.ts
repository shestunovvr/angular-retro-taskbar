import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-column-form',
  templateUrl: './add-column-form.component.html',
  styleUrls: ['./add-column-form.component.scss']
})
export class AddColumnFormComponent implements OnInit {
  @Output() onSubmit: EventEmitter<{title: string, color: string, bgColor: string}> = new EventEmitter<{title: string, color: string, bgColor: string}>()
  @Output() onClose: EventEmitter<void> = new EventEmitter<void>()
  form: FormGroup
  constructor() { }

  ngOnInit(): void {
    this.setForm();
  }

  submit(): void {
    if(this.form.valid) {
      this.onSubmit.emit(
        {
          title: this.form.value.text,
          color: this.form.value.color.toString(),
          bgColor: this.form.value.bgColor.toString()
        }
      );
      this.form.reset();
    }
  }

  closeForm(): void {
    this.onClose.emit();
  }

  setForm(): void {
    this.form = new FormGroup({
      text: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      bgColor: new FormControl('#231f20', [
        Validators.required
      ]),
      color: new FormControl('#ffffff', [
        Validators.required
      ])
    })
  }
}
