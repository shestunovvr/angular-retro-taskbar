import {Component, OnDestroy, OnInit} from '@angular/core';
import {Column, Item} from "../shared/interfaces";
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {
  collection,
  Firestore, getDocs
} from "@angular/fire/firestore";
import {FirestoreUpdaterService} from "../services/firestore-updater.service";
import {Subscription} from "rxjs";
import {AuthService} from "../services/auth.service";
import {HomePageHandlerService} from "../services/home-page-handler.service";
import {DownloaderService} from "../services/downloader.service";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  showAddColForm = false
  currentUser: string | null = ''
  currentUserSubscription: Subscription
  possibleStatuses: string[] = []
  needToLogin: boolean
  needToLoginMessage: string
  downloadedColumns: any[] = []
  columns: Column[]
  loading = false

  constructor(
    private store: Firestore,
    private firestoreUpdater: FirestoreUpdaterService,
    public auth: AuthService,
    private eventHandlerService: HomePageHandlerService,
    private dowloadService: DownloaderService
  ) {
  }

  async ngOnInit(): Promise<void> {
    await this.setColumns();
    this.updateStatuses();
    this.setCurrentUserSubscription();
  }

  ngOnDestroy(): void {
    this.currentUserSubscription.unsubscribe();
  }

  toggleShowComments(event: { item: Item, column: Column }): void {
    this.columns = this.eventHandlerService.toggleCommentEventHandler(this.columns, event);
  }

  toggleAddColForm(): void {
    if (this.currentUser) {
      this.showAddColForm = !this.showAddColForm;
    }
  }

  addColumn(event: {title: string, color: string, bgColor: string}): void {
    if (this.currentUser) {
      this.columns = this.eventHandlerService.addColumnEventHandler(this.columns, event);
      this.possibleStatuses.push(event.title.split(' ').join(''));
      this.showAddColForm = !this.showAddColForm;
    }
  }

  submitItem(event: { text: string, column: Column }): void {
    if (this.currentUser) {
      this.columns = this.eventHandlerService.submitNewItemHandler(this.columns, event);
    }
  }

  likeFunc(event: {item: Item, column: Column}): void {
    if (this.currentUser) {
      this.columns = this.eventHandlerService.likeEventHandler(this.columns, event, this.currentUser);
    } else {
      this.showModal('like');
    }
  }

  commentSubmit(event: {id: string, commentText: string, columnTitle: string}): void {
    if (this.currentUser) {
      this.columns = this.eventHandlerService.submitCommentHandler(this.columns, event, this.currentUser);
    }
  }

  drop(event: CdkDragDrop<Item[]>): void {
    if (this.currentUser) {
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      } else {
        transferArrayItem(
          event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex,
        );
      }
      this.firestoreUpdater.updateAllColumns(this.columns);
    } else {
      this.showModal('drag');
    }
  }

  showModal(text: string): void {
    this.needToLogin = true;
    switch (text) {
      case 'drag': {
        this.needToLoginMessage = 'You have to log in into the system to change task status!';
        break;
      }
      case 'like': {
        this.needToLoginMessage = 'You have to log in into the system to like posts!';
        break;
      }
    }
    setTimeout(() => this.needToLogin = false, 3000)
  }

  updateStatuses(): void {
    this.columns.forEach(col => {
      this.possibleStatuses.push(col.class);
    });
  }

  setCurrentUserSubscription(): void {
    this.currentUserSubscription = this.auth.currentUser.subscribe(user => {
      this.currentUser = user;
    })
  }

  async setColumns(): Promise<void> {
      try {
        this.loading = true;
        let columnsResponse = await getDocs(collection(this.store, 'columns'));
        columnsResponse.forEach(el => {
          this.downloadedColumns.push(el.data());
        });
        if (this.downloadedColumns.length > 0) {
          this.columns = this.downloadedColumns;
        }
        this.columns.forEach(col => {
          this.possibleStatuses.push(col.class);
        })
        this.loading = false;
      } catch (e) {
        console.log(e.message);
        this.loading = false;
      }
  }

  download() {
    this.dowloadService.downloadJSON(this.columns);
  }
}
