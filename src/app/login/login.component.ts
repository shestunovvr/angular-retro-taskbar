import {Component, OnInit} from '@angular/core';
import {User} from "../shared/interfaces";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  users: User[] = []
  hasAccount = true
  form: FormGroup

  constructor(
    public auth: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.setForm();
  }

  toggleHasAccount(): void {
    this.hasAccount = !this.hasAccount;
  }

  submit(): void {
    if (this.hasAccount) {
      this.login();
    } else {
      this.register();
    }
    this.form.reset();
  }

  login(): void {
    this.auth.login({
      email: this.form.value.email,
      password: this.form.value.password,
      userName: ''
    });
  }

  register(): void {
    this.auth.register({
      email: this.form.value.email,
      password: this.form.value.password,
      userName: this.form.value.userName
    });
  }

  setForm(): void {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.email,
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      userName: new FormControl('',)
    })
  }
}
