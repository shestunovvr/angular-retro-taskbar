import {Injectable, OnInit} from '@angular/core';
import {User} from "../shared/interfaces";
import {Router} from "@angular/router";
import {addDoc, collection, Firestore, getDocs} from "@angular/fire/firestore";
import {
  Auth,
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  updateProfile
} from "@angular/fire/auth";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: "root"
})

export class AuthService  {
  user$: Observable<User | null>
  wrongUser = false
  currentUser = new BehaviorSubject(localStorage.getItem('username'))

  constructor(
    private router: Router,
    private store: Firestore,
    private auth: Auth
  ) {
    this.user$ = new Observable((observer: any) =>
      onAuthStateChanged(auth, observer)
    );
  }

  async register(incomeUser: User): Promise<void> {
    try {
      const credential = await createUserWithEmailAndPassword(
        this.auth,
        incomeUser.email,
        incomeUser.password
      );
      await updateProfile(
        credential.user, {displayName: credential.user.displayName}
      );
      await this.addUser(incomeUser, "users");
      this.router.navigate(['']);
    } catch (e) {
      console.log(e);
    }
  }

  async login(user:User): Promise<void> {
    try{
      const response = await signInWithEmailAndPassword(this.auth, user.email, user.password);
      await this.setUser(response.user.email, 'users');
      this.router.navigate(['']);
    } catch (e) {
      this.showWrongUserBanner();
    }
  }

  async addUser (user: User, path: string): Promise<void> {
    await addDoc(collection(this.store, path), {
      email: user.email,
      password: user.password,
      userName: user.userName
    });
    this.saveUserToLocalStorage(user.userName);
  }

  async setUser(email: string | null, path: string): Promise<void> {
    const users = await getDocs(collection(this.store, path));
    users.forEach(el => {
      if(el.data().email === email) {
        this.saveUserToLocalStorage(el.data().userName);
      }
    })
  }

  showWrongUserBanner(): void {
    this.wrongUser = !this.wrongUser;
    setTimeout(() => {
      this.wrongUser = !this.wrongUser
    }, 3000)
  }

  saveUserToLocalStorage(username: string): void {
    localStorage.setItem('username', username);
    this.currentUser.next(username);
  }

  logout(): void {
    localStorage.clear();
    this.currentUser.next('');
  }
}
