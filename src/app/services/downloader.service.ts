import { Injectable } from '@angular/core';
import {Column} from "../shared/interfaces";
import * as XLSX from "xlsx";
import * as fileSaver from "file-saver";

@Injectable({
  providedIn: 'root'
})
export class DownloaderService {
  excelType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8'
  constructor() { }

  downloadJSON(columns: Column[]): void {
    const excelTemplateObject = this.setObjectToExcelTemplate(columns);
    const excelBuffer = this.createExcel(excelTemplateObject);
    this.saveAsExcelFile(excelBuffer, 'test', 'xlsx');
  }

  setObjectToExcelTemplate(columns: Column[]): any[] {
    let headers: any[] = [];
    columns.forEach(column => {
      let newEl: any = {
        'Column tile': column.title
      }
      column.elements.forEach((el, index) => {
        let idx = index + 1;
        newEl['Column ' + ' task ' + idx] = el.text;
      })
      headers.push(newEl);
    })
    return headers;
  }

  createExcel(data: any): any {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
    const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    return excelBuffer;
  }

  saveAsExcelFile(incomeData: any, fileName: string, fileType: string): void {
    const data: Blob = new Blob([incomeData], {type: this.excelType});
    fileSaver.saveAs(data, fileName);
  }
}
