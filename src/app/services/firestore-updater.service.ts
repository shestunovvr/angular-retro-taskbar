import { Injectable } from '@angular/core';
import {doc, Firestore, setDoc, updateDoc} from "@angular/fire/firestore";
import {Column} from "../shared/interfaces";

@Injectable({
  providedIn: 'root'
})
export class FirestoreUpdaterService {

  constructor(
    private store: Firestore
  ) { }

  async addColumn(newColumn: Column): Promise<void> {
    try {
      await setDoc(doc(this.store, 'columns', newColumn.title), newColumn);
    } catch (e) {
      console.log(e.message);
    }
  }

  async updateOneColumn(title: string, columnToUpdate: Column): Promise<void> {
    try{
      const thisCol = doc(this.store, 'columns', title);
      await updateDoc(thisCol, columnToUpdate);
    } catch (e) {
      console.log(e.message);
    }
  }

  async updateAllColumns(columns: Column[]): Promise<void> {
    try {
      columns.forEach(async (column) => {
        const thisCol = doc(this.store, 'columns', column.title);
        await updateDoc(thisCol, column);
      })
    } catch (e) {
      console.log(e.message);
    }
  }
}
