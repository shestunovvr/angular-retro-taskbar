import { Injectable } from '@angular/core';
import {Column, Item} from "../shared/interfaces";
import {FirestoreUpdaterService} from "./firestore-updater.service";

@Injectable({
  providedIn: 'root'
})
export class HomePageHandlerService {

  constructor(
    private firestore: FirestoreUpdaterService,
  ) { }

  likeEventHandler(
    columns: Column[],
    event: {item: Item, column: Column},
    currentUser: string
  ): Column[] {
    let thisColIndex = 0;
    columns.forEach((column, index) => {
      if (column.title === event.column.title) {
        thisColIndex = index;
        column.elements.forEach(task => {
          if (task.id === event.item.id) {
            if (currentUser && !task.likedBy.includes(currentUser)) {
              task.likedBy.push(currentUser);
            } else {
              let newArr: string[] = [];
              task.likedBy.forEach(el => {
                if (el !== currentUser) {
                  newArr.push(el);
                }
              })
              task.likedBy = newArr;
            }
          }
        })
      }
    });
    this.firestore.updateOneColumn(columns[thisColIndex].title, columns[thisColIndex]);
    return columns;
  }

  toggleCommentEventHandler(
    columns: Column[],
    event: {item: Item, column: Column}
  ): Column[] {
    columns.forEach(column => {
      if (column.title === event.column.title) {
        column.elements.forEach(task => {
          if (task.id === event.item.id) {
            task.showComments = !task.showComments;
          }
        })
      }
    });
    return columns;
  }

  addColumnEventHandler(
    columns: Column[],
    event: {title: string, color: string, bgColor: string}
  ): Column[] {
    const newCol = {
      title: event.title,
      class: event.title.split(' ').join(''),
      color: event.color,
      bgColor: event.bgColor,
      elements: []
    }
    columns.push(newCol);
    this.firestore.addColumn(newCol);
    return columns;
  }

  submitNewItemHandler(
    columns: Column[],
    event: { text: string, column: Column }
  ): Column[] {
    let thisColIndex = 0;
    columns.forEach((column, index) => {
      if (column.title === event.column.title) {
        thisColIndex = index;
        column.elements.push({
          id: Math.random().toString(),
          text: event.text,
          showComments: false,
          comments: [],
          likedBy: []
        });
      }
    });
    this.firestore.updateOneColumn(columns[thisColIndex].title, columns[thisColIndex]);
    return columns;
  }

  submitCommentHandler(
    columns: Column[],
    event: {id: string, commentText: string, columnTitle: string},
    currentUser: string
  ): Column[] {
    let thisColIndex = 0;
    columns.forEach((column, index) => {
      if (column.title === event.columnTitle) {
        thisColIndex = index;
        column.elements.forEach(task => {
          if (task.id === event.id) {
            task.comments.push({
              commentText: event.commentText,
              author: currentUser,
              dated: new Date().toString()
            });
            task.showComments = !task.showComments;
          }
        })
      }
    })
    this.firestore.updateOneColumn(columns[thisColIndex].title, columns[thisColIndex]);
    return columns;
  }
}
