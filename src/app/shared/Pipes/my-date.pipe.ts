import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myDate'
})
export class MyDatePipe implements PipeTransform {

  transform(value: string): string {
    let date = value.slice(0, 15);
    let time = new Date(value).toLocaleTimeString()
    return time + ' ' + date;
  }

}
