import {Directive, ElementRef, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appItemColors]'
})
export class ItemColorsDirective {
  @Input() dStyles: {color: string, bgColor: string}

  constructor(private elRef: ElementRef, private renderer: Renderer2) {
  }

  @HostListener('mouseenter') setInvertedColors(): void {
    this.renderer.setStyle(this.elRef.nativeElement, 'color', this.dStyles.bgColor);
    this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', this.dStyles.color);
  }

  @HostListener('mouseleave') setBaseColors(): void {
    this.renderer.setStyle(this.elRef.nativeElement, 'color', this.dStyles.color);
    this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', this.dStyles.bgColor);
  }
}
