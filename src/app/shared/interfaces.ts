export interface User {
  email: string
  password: string
  userName: string
  returnSecureToken?: boolean
  accessToken?: string
}

export interface Column {
  title: string
  class: string
  color?: string
  bgColor?: string
  id?: string
  elements: Item[]
}

export interface Item {
  id: string
  text: string
  showComments: boolean
  comments: Comment[]
  likedBy: string[]
}

export interface Comment {
  author: string | null
  commentText: string
  dated: string
}
